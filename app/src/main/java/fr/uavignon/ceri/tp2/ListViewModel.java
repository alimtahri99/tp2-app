package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class ListViewModel extends AndroidViewModel {
    private BookRepository repository;
    private LiveData<List<Book>> allBooks;

    public ListViewModel(@NonNull Application application) {
        super(application);
        repository = new BookRepository(application);
        allBooks=repository.getAllBook();
    }

    public void insert(Book book) {
        repository.insertBook(book);
    }
    public void update(Book book) {
        repository.updateBook(book);
    }
    public void delete(Book book) {
        repository.deleteBook(book);
    }
    public void deleteAllBooks() {
        repository.deleteAllBooks();
    }
    public LiveData<List<book>> getAllBooks() {
        return allBooks;
    }
}
