package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp2.data.Book;

import static fr.uavignon.ceri.tp2.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {


    private MutableLiveData<List<Book>> selectedBook =
            new MutableLiveData<>();
    private LiveData<List<Book>> allBooks;

    private DAOBookDao productDao;

    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase .getDatabase(application);
        productDao = db.bookDao();
        allBooks = productDao.getAllBooks();
    }

    public void insertBook(Book book) {
        databaseWriteExecutor.execute(() -> {
            productDao.insertBook(book);
        });
    }



    public void updateBook(Book book) {
        databaseWriteExecutor.execute(() -> {
            productDao.updateBook(book);
        });
    }

    public void deleteBook(long id) {
        databaseWriteExecutor.execute(() -> {
            productDao.deleteBook(id);
        });
    }


    public void deleteAllBooks() {
        databaseWriteExecutor.execute(() -> {
            productDao.deleteAllBooks();
        });
    }

    public void getBook(long id) {

        Future<List<Book>> fproducts = (Future<List<Book>>) databaseWriteExecutor.submit(() -> {
            return productDao.getBook(id);
        });
        try {
            selectedBook.setValue(fproducts.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public LiveData<List<Book>> getAllBook() {
        return allBooks;
    }

    public MutableLiveData<List<Book>> getSearchResults() {
        return selectedBook;
    }
}
